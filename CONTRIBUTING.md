# Contribuindo

Merge Requests são bem vindos, incluindo refatoração do código, correção de bugs ou novas funcionalidades. Considere abrir antes uma issue para qualquer um dos casos.

## Guidelines

- Utilize descrição de commits em português (é um projeto educacional) com até 72 caracteres
- Descreva a implementação em caso de Merge Request e referêncie uma issue