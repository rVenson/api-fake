# API Fake para Integração com Frontend

Este projeto é usado como modelo para a implementação flexível de um frontend com integração à uma API RESTful usando Node.js, Express e Mongoose. O projeto foi implementado para uso educacional.

## Instalação

Para instalar o projeto, basta clonar o código e executar a instalação usando o NPM ou equivalente:

````sh
npm install
````

### Variáveis de ambiente

É necessário definir variáveis de ambiente do sistema ou criar um arquivo `.env` na raiz do projeto contendo algumas variáveis de ambiente. Um modelo pode ser encontrado em `/.env.example`. O pacote usado para leitura é o [dotenv](https://www.npmjs.com/package/dotenv).

| Variável | Descrição |
| --- | --- |
| PORT | Porta a ser utilizada pelo express para rodar o web server (Padrão: 3000 se não for definido) |
| MONGOOSE_URI | URI completa para conexão com um banco de dados MongoDB (Requerido).  > O banco deve ser inicializado a parte. Considere instalar a [versão Community do MongoDB](https://www.mongodb.com/try/download/community), usar um [container docker](https://hub.docker.com/_/mongo) ou o serviço [cloud da Atlas](cloud.mongodb.com/)   |
| API_URL | URL completa da API usada pelo documentação do Swagger |

## Rodando a aplicação

Após configurada, a aplicação pode ser exucutada em ambiente de desenvolvimento usando o NPM:

````sh
npm run dev
````

Ou iniciada em ambiente de produção usando:

````sh
npm start
````

## Rotas

A API permite a manipulação de documentos flexíveis (sem definição estrita de atributos). Pode-se utilizar duas diferentes rotas para manipular documentos:

- `/api/v1/{usuario}` - Integração com banco de dados MongoDB usando o mongoose. Os dados são persistidos na base de dados
- `/api/{usuario}` - Usa implementação simples com arrays. Não possui persistência de dados.

* `{usuario}` corresponde a um código de estudante para separação dos dados dos estudantes em diferentes endpoints

Ambas as rotas possuem os métodos:

- `GET /` - Retorna a lista de documentos do usuario
- `GET /{id}` - Retorna um objeto específico, onde `{id}` é o identificador único (`_id`), no caso da implementação com o mongoose, ou o índice do vetor, no caso da implementação com arrays.
- `POST /` - Insere um novo documento na lista. Deve-se passar no corpo da mensagem um objeto JSON válido contendo pelo menos um atributo.
- `PUT /{id}` - Atualiza um documento na lista onde `{id}` é o identificador único (`_id`), no caso da implementação com o mongoose, ou o índice do vetor, no caso da implementação com arrays. Deve-se passar no corpo da mensagem um objeto JSON válido contendo pelo menos um atributo.
- `DELETE /{id}` - Deleta um documento na lista onde `{id}` é o identificador único (`_id`), no caso da implementação com o mongoose, ou o índice do vetor, no caso da implementação com arrays.
- `DELETE /` - Deleta todos os documentos do usuário.

> Repare que a rota completa deve conter o caminho completo dependendo da implementação a ser usada: `/api/v1/{usuario}/{id}` ou `/api/{usuario}/{id}`.

### Exemplos

- Receber a lista de todos os documentos no banco do usuário 9999: `GET /api/v1/9999`
- Inserir um novo documento com o atributo nome no banco do usuário 9999: `POST /api/v1/9999 BODY {"nome": "fulano"}`
- Receber o item da posição 5 do array do usuário 9999: `GET /api/9999/5`
- Deletar todos os items do array do usuário 9999: `DELETE /api/9999`

## Funcionalidades

- Integração com banco de dados MongoDB
- Implementação de CRUD usando apenas arrays, sem persistência de dados
- Documentação com Swagger