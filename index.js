require('dotenv').config()
require('express-async-errors')
const docs = require('./docs')
const express = require("express")
const app = express()
const cors = require('cors')
const routes = require('./api/routes')
const errorHandler = require('./api/middlewares/errorHandler')

app.use(cors())
app.get('/', function(req, res){
    res.send("A API FAKE está online!")
})

app.use('/docs', docs)
app.use('/api', routes)
app.use(errorHandler)

app.listen(process.env.PORT || 3000)