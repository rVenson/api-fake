module.exports = function errorHandler(err, req, res, next){
    console.error(err.message)
    if(res.statusCode == 200){
        res.statusCode = 500
        err.message = "Algo de errado não está certo. Tivemos um problema no servidor. Contate o administrador se o problema persistir"
    }
    res.json({
        "error": err.message
    })
}