module.exports = (req, res, buf, encoding) => {
    try {
        JSON.parse(buf);
    } catch(e) {
        res.statusCode = 400
        throw Error('O corpo da mensagem precisa ser um objeto JSON válido');
    }
}