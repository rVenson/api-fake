const express = require('express')
const route = express.Router()
const mongoose = require('mongoose')
const GenericData = require('../model/GenericData')
const bodyVerification = require('../middlewares/bodyVerification')

mongoose.connect(process.env.MONGOOSE_URI).then(() => {
    console.log("Rota conectada com o banco de dados com sucesso!")
})

route.use(express.json({verify: bodyVerification}))

route.get('/:user', async function(req, res){
    const userCode = req.params.user
    const result = await GenericData.find({code: userCode})
    res.json(result)
})

route.get('/:user/:id', async function(req, res){
    const userCode = req.params.user
    const id = req.params.id
    const result = await GenericData.findById(id)
    if(result.length == 0){
        res.statusCode = 404
        throw new Error(`Nenhum dado encontrado com este código (${id})`)
    }
    res.json(result)
})

route.post('/:user', async function(req, res){
    const userCode = req.params.user
    const payload = req.body
    if(!payload || typeof payload != 'object' || Object.keys(payload).length == 0){
        res.statusCode = 400
        throw new Error(`O corpo da mensagem precisa conter pelo menos um atributo`)
    }
    payload.code = userCode
    const user = new GenericData(payload)
    const result = await user.save()
    res.json(result)
})

route.put('/:user/:id', async function(req, res){
    const id = req.params.id
    if(!mongoose.isValidObjectId(id)){
        res.statusCode = 400
        throw new Error(`O id informado não está em um formato válido (${id})`)
    }
    const payload = req.body
    if(!payload || typeof payload != 'object' || Object.keys(payload).length == 0){
        res.statusCode = 400
        throw new Error(`O corpo da mensagem precisa conter pelo menos um atributo`)
    }
    const result = await GenericData.findByIdAndUpdate(id, payload)
    if(!result){
        res.statusCode = 404
        throw new Error(`Nenhum dado encontrado com este código (${id})`)
    }
    res.json(result)
})

route.delete('/:user/:id', async function(req, res){
    const id = req.params.id
    const result = await GenericData.findByIdAndDelete(id)
    if(!result){
        res.statusCode = 404
        throw new Error(`Nenhum dado encontrado neste índice (${id})`)
    }
    res.json(result)
})

route.delete('/:user', async function(req, res){
    const userCode = req.params.user
    const result = await GenericData.deleteMany({code: userCode})
    res.json(result)
})

module.exports = route