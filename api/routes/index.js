const express = require('express')
const router = express.Router()
const routes = {
    GenericArrayData: require('./GenericArrayData.route'),
    GenericData: require('./GenericData.route')
}

router.use('/v1', routes.GenericData)
router.use('/', routes.GenericArrayData)

module.exports = router