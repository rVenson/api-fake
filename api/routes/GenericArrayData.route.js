const express = require('express')
const route = express.Router()
const GenericArrayData = require('../model/GenericArrayData')
const bodyVerification = require('../middlewares/bodyVerification')

route.use(express.json({verify: bodyVerification}))

route.get('/:user', function(req, res){
    const result = GenericArrayData.getAll(req.params.user)
    res.json(result)
})

route.get('/:user/:id', function(req, res){
    const id = req.params.id
    const result = GenericArrayData.get(req.params.user, id)
    if(!result){
        res.statusCode = 404
        throw new Error(`Nenhum dado encontrado neste índice (${id})`)
    }
    res.json(result)
})

route.post('/:user', function(req, res){
    const payload = req.body
    if(!payload || typeof payload != 'object' || Object.keys(payload).length == 0){
        res.statusCode = 400
        throw new Error(`O corpo da mensagem precisa conter pelo menos um atributo`)
    }
    const result = GenericArrayData.save(req.params.user, payload)
    res.json(result)
})

route.put('/:user/:id', function(req, res){
    const payload = req.body
    if(!payload || typeof payload != 'object' || Object.keys(payload).length == 0){
        res.statusCode = 400
        throw new Error(`O corpo da mensagem precisa conter pelo menos um atributo`)
    }
    const result = GenericArrayData.save(req.params.user, payload, req.params.id)
    res.json(result)
})

route.delete('/:user/:id', function(req, res){
    const id = req.params.id
    const result = GenericArrayData.delete(req.params.user, id)
    if(result.length == 0){
        res.statusCode = 404
        throw new Error(`Nenhum dado encontrado neste índice (${id})`)
    }
    res.json(result)
})

route.delete('/:user', function(req, res){
    const result = GenericArrayData.deleteAll(req.params.user)
    res.json({
        deletedCount: result
    })
})

module.exports = route