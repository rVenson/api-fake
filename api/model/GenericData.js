const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    code: String
}, {timestamps: true, strict: false})

schema.methods.toJSON = function() {
    const obj = this.toObject();
    delete obj.__v;
    delete obj.code;
    return obj;
}

const model = mongoose.model('GenericData', schema)

module.exports = model