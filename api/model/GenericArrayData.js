var GenericArrayData = {}
var dados = {
    "113947": [],
    "19512": [],
    "103754": [],
    "111978": [],
    "117565": [],
    "105933": [],
    "67463": [],
    "112608": [],
    "27314": [],
    "112856": [],
    "115939": [],
    "110282": [],
    "93527": [],
    "113060": [],
    "113602": [],
    "15293": [],
    "113375": [],
    "96936": [],
    "113345": [],
    "99195": [],
    "107234": [],
    "65472": [],
    "83031": [],
    "117544": [],
    "20796": [],
    "123464": [],
    "15293": [],
    "126393": [],
    "65472": [],
    "108951": [],
    "119706": [],
    "117403": [],
    "113602": [],
    "114354": [],
    "123696": [],
    "118589": [],
    "89012": [],
    "117444": [],
    "103754": [],
    "111895": [],
    "99195": []
}

GenericArrayData.getAll = function (user) {
    return dados[user]
}

GenericArrayData.get = function (user, id) {
    return dados[user][id]
}

GenericArrayData.save = function (user, payload, id = null) {
    if (id == null) {
        dados[user].push(payload)
    } else {
        dados[user][id] = payload
    }
    return payload
}

GenericArrayData.delete = function (user, id) {
    var deleted = dados[user].splice(id, 1)
    return deleted
}

GenericArrayData.deleteAll = function (user) {
    const lastLenghth = dados[user].length
    dados[user].length = 0
    return lastLenghth
}

module.exports = GenericArrayData