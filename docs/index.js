const express = require('express')
const router = express.Router()
const swaggerJsdoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express")

const options = {
    definition: {
        title: "API Fake - PPW I",
        openapi: "3.0.0",
        info: {
            title: "Projeto Final PPW I - API Fake",
            version: "0.1.0",
            description: "Esta API é um protótipo para implementação de frontends usada na disciplina de Programação para Web I, no Curso de Ciência da Computação da Universidade do Extremo Sul Catarinense (UNESC)",
            license: {
                name: "GPL-3.0",
                url: "https://www.gnu.org/licenses/gpl-3.0.pt-br.html"
            },
            contact: {
                name: "Ramon Venson",
                url: "https://venson.net.br",
                email: "ramon.venson@unesc.net"
            }
        },
        servers: [
            {
                url: process.env.API_URL
            },
        ],
        paths: {
            "/v1/{user}": {
                "parameters": [
                    {
                        "name": "user",
                        "in": "path",
                        "required": true,
                        "description": "Código do estudante. Aceita qualquer valor",
                        "type": "string",
                        "example": "99999"
                    }
                ],
                "get": {
                    "tags": ["v1"],
                    "summary": "Lista contendo todos os documentos de um determinado usuário (código do estudante)",
                    "responses": {
                        "200": {
                            "description": "Retorna uma lista de usuários. Retorna um vetor vazio caso não haja dados naquele código",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericDataList"
                                    }
                                }
                            }
                        }
                    }
                },
                "post": {
                    "tags": ["v1"],
                    "summary": "Insere um novo documento na lista do estudante",
                    "requestBody":{
                        "description": "Os dados personalizados pelo estudante",
                        "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": '#/definitions/GenericData'
                                },
                                "example": {
                                    "atributo1": "valorExemplo",
                                    "atributo2": 50,
                                    "atributo3": false
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "Retorna o documento que foi cadastrado no banco de dados",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericData"
                                    }
                                }
                            }
                        }
                    }
                },
                "delete": {
                    "tags": ["v1"],
                    "summary": "Deleta TODOS os documentos da lista do estudante (usar com cautela!)",
                    "responses": {
                        "200": {
                            "description": "Retorna o número de documentos deletados",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "properties": {
                                            "deletedCount": {
                                                "type": "Number",
                                                "example": "7"
                                            },
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "/v1/{user}/{id}": {
                "parameters": [
                    {
                        "name": "user",
                        "in": "path",
                        "required": true,
                        "description": "Código do estudante. Aceita qualquer valor",
                        "type": "string",
                        "example": "99999"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Identificador único de um documento",
                        "type": "string",
                        "example": "5d6ede6a0ba62570afcedd3a"
                    }
                ],
                "get": {
                    "tags": ["v1"],
                    "summary": "Retorna um documento de um determinado usuário (código do estudante) e identificador único (id)",
                    "responses": {
                        "200": {
                            "description": "Retorna o documento caso seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "Retorna um erro (Bad Request) caso o {id} não seja um ObjectId válido",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        }
                    }
                },
                "put": {
                    "tags": ["v1"],
                    "summary": "Atualiza um documento de acordo com o {id} e código do usuário",
                    "requestBody":{
                        "description": "Os dados personalizados pelo estudante",
                        "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": '#/definitions/GenericData'
                                },
                                "example": {
                                    "atributo1": "valorExemplo",
                                    "atributo2": 50,
                                    "atributo3": false
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "Retorna o documento que foi sobreescrito (antes de ser atualizado)",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        }
                    }
                },
                "delete": {
                    "tags": ["v1"],
                    "summary": "Deleta um documento de acordo com o {id} e código do usuário",
                    "responses": {
                        "200": {
                            "description": "Retorna o documento deletado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        },
                    }
                }
            },
            "/{user}": {
                "parameters": [
                    {
                        "name": "user",
                        "in": "path",
                        "required": true,
                        "description": "Código do estudante. Aceita apenas códigos válidos",
                        "type": "string",
                        "example": "99999"
                    }
                ],
                "get": {
                    "tags": ["default"],
                    "summary": "Lista contendo todos os documentos de um determinado usuário (código do estudante)",
                    "responses": {
                        "200": {
                            "description": "Retorna uma lista de usuários. Retorna um vetor vazio caso não haja dados naquele código",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericArrayDataList"
                                    }
                                }
                            }
                        }
                    }
                },
                "post": {
                    "tags": ["default"],
                    "summary": "Insere um novo documento na lista do estudante",
                    "requestBody":{
                        "description": "Os dados personalizados pelo estudante",
                        "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": '#/definitions/GenericData'
                                },
                                "example": {
                                    "atributo1": "valorExemplo",
                                    "atributo2": 50,
                                    "atributo3": false
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "Retorna o documento que foi cadastrado no banco de dados",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericArrayData"
                                    }
                                }
                            }
                        }
                    }
                },
                "delete": {
                    "tags": ["default"],
                    "summary": "Deleta TODOS os documentos da lista do estudante (usar com cautela!)",
                    "responses": {
                        "200": {
                            "description": "Retorna o número de documentos deletados",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "properties": {
                                            "deletedCount": {
                                                "type": "Number",
                                                "example": "7"
                                            },
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "/{user}/{id}": {
                "parameters": [
                    {
                        "name": "user",
                        "in": "path",
                        "required": true,
                        "description": "Código do estudante. Aceita apenas códigos válidos",
                        "type": "string",
                        "example": "99999"
                    },
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "description": "Identificador único de um documento",
                        "type": "string",
                        "example": "5d6ede6a0ba62570afcedd3a"
                    }
                ],
                "get": {
                    "tags": ["default"],
                    "summary": "Retorna um documento de um determinado usuário (código do estudante) e identificador único (id)",
                    "responses": {
                        "200": {
                            "description": "Retorna o documento caso seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericArrayData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "Retorna um erro (Bad Request) caso o {id} não seja um ObjectId válido",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        }
                    }
                },
                "put": {
                    "tags": ["default"],
                    "summary": "Atualiza um documento de acordo com o {id} e código do usuário",
                    "requestBody":{
                        "description": "Os dados personalizados pelo estudante",
                        "required": true,
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": '#/definitions/GenericData'
                                },
                                "example": {
                                    "atributo1": "valorExemplo",
                                    "atributo2": 50,
                                    "atributo3": false
                                }
                            }
                        }
                    },
                    "responses": {
                        "200": {
                            "description": "Retorna o documento que foi sobreescrito (antes de ser atualizado)",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericArrayData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/Error"
                                    }
                                }
                            }
                        }
                    }
                },
                "delete": {
                    "tags": ["default"],
                    "summary": "Deleta um documento de acordo com o {id} e código do usuário",
                    "responses": {
                        "200": {
                            "description": "Retorna o documento deletado",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/definitions/GenericArrayData"
                                    }
                                }
                            }
                        },
                        "404": {
                            "description": "Retorna um erro (Not Found) caso o documento não seja encontrado",
                            "content": {
                                "application/json": {
                                    "properties": {
                                        "deletedCount": {
                                            "type": "Number",
                                            "example": "7"
                                        },
                                    }
                                }
                            }
                        },
                    }
                }
            }
        },
        "tags": [
            {
                "name": "v1",
                "description": "Essa versão da API foi implementada com o uso de um banco de dados MongoDB. Isso significa que todas as operações são persistidas. Além disso, o acesso aos dados é PÚBLICO e a manipulação de qualquer dado sensivel deve ser evitado aqui."
            },
            {
                "name": "default",
                "description": "Essa versão da API foi implementada com o uso de arrays simples. Os dados não são persistidos. Além disso, o acesso aos dados é PÚBLICO e a manipulação de qualquer dado sensivel deve ser evitado aqui."
            }
        ],
        "definitions": {
            "GenericData": {
                "properties": {
                    "_id": {
                        "type": "string",
                        "pattern": "/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i",
                        "uniqueItems": true,
                        readOnly: true
                    },
                    "updatedAt": {
                        "type": "string",
                        readOnly: true
                    },
                    "createdAt": {
                        "type": "string",
                        readOnly: true
                    }
                },
                example: {
                    "_id": "5d6ede6a0ba62570afcedd3a",
                    "updatedAt": "2021-11-24T09:57:24.384Z",
                    "createdAt": "2021-11-24T09:57:24.384Z",
                    "atributo1": "valorExemplo",
                    "atributo2": 50,
                    "atributo3": false
                }
            },
            "GenericArrayData": {
                AnyValue: {
                    description: "Qualquer valor"
                },
                example: {
                    "atributo1": "valorExemplo",
                    "atributo2": 50,
                    "atributo3": false
                }
            },
            "GenericDataList": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/GenericData"
                }
            },
            "GenericArrayDataList": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/GenericArrayData"
                }
            },
            "Error": {
                "properties": {
                    "error": {
                        "type": "string"
                    }
                }
            }
        }
    },
    "apis": ["./api/routes/GenericData"]
}

const specs = swaggerJsdoc(options)
router.use(swaggerUi.serve, swaggerUi.setup(specs, {swaggerOptions: { docExpansion: "none"}}))

module.exports = router
